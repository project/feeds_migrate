<?php

namespace Drupal\feeds_migrate\Entity;

use Drupal\migrate_plus\Entity\MigrationInterface as MigratePlusMigrationInterface;

/**
 * Extends Migrate Plus's migration interface with extra methods.
 */
interface MigrationInterface extends MigratePlusMigrationInterface {

  /**
   * Get the destination configuration.
   *
   * @return array
   *   The destination configuration.
   */
  public function getDestinationConfiguration();

  /**
   * Get the source configuration.
   *
   * @return array
   *   The source configuration.
   */
  public function getSourceConfiguration();

  /**
   * Adds a source a field.
   *
   * @param string $name
   *   The field to set.
   * @param array $values
   *   (optional) The values to set for the source field. The key 'name' will
   *   always equal the given source field name.
   *   Defaults to 'name', 'label' and 'selector' all being set to the field
   *   name.
   *
   * @return $this
   *   An instance of this class.
   */
  public function addSourceField(string $name, array $values = []): MigrationInterface;

  /**
   * Removes a source a field.
   *
   * @param string $name
   *   The field to remove.
   *
   * @return $this
   *   An instance of this class.
   */
  public function removeSourceField(string $name): MigrationInterface;

  /**
   * Marks a field as unique.
   *
   * @param string $name
   *   The field to set.
   * @param array $values
   *   (optional) The values to set for the field.
   *   Defaults to [type => string].
   *
   * @return $this
   *   An instance of this class.
   */
  public function addIdField(string $name, array $values = []): MigrationInterface;

  /**
   * Removes a field that was marked as unique.
   *
   * @param string $name
   *   The field to remove.
   *
   * @return $this
   *   An instance of this class.
   */
  public function removeIdField(string $name): MigrationInterface;

  /**
   * Find the entity type this migration will import into.
   *
   * @return string
   *   Machine name of the entity type eg 'node'.
   */
  public function getEntityTypeIdFromDestination();

  /**
   * The bundle the migration is importing into.
   *
   * @return string|null
   *   Entity type bundle eg 'article' or null if no bundle is defined.
   */
  public function getEntityBundleFromDestination(): ?string;

  /**
   * Find the field this migration mapping is pointing to.
   *
   * @param string $field_name
   *   The name of the field to look for.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|null
   *   The field definition - if any.
   */
  public function getDestinationField($field_name);

  /**
   * Get a list of fields for the destination the this migration is pointing at.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   The array of field definitions for the bundle, keyed by field name.
   */
  public function getDestinationFields();

  /**
   * Gets all migration mappings.
   *
   * @return array
   *   List of migration mappings.
   */
  public function getMappings();

  /**
   * Sets a single migration mapping.
   *
   * @param string $destination_key
   *   The mapping to set.
   * @param array $mapping
   *   A single mapping.
   *
   * @return $this
   *   An instance of this class.
   */
  public function setMapping($destination_key, array $mapping);

  /**
   * Sets all migration mappings.
   *
   * @param array $mappings
   *   A list of mappings.
   *
   * @return $this
   *   An instance of this class.
   */
  public function setMappings(array $mappings);

  /**
   * Removes a mapping from the migration.
   *
   * @param string $destination_key
   *   The mapping to remove.
   *
   * @return $this
   *   An instance of this class.
   */
  public function removeMapping($destination_key);

  /**
   * Removes all mappings.
   *
   * @return $this
   *   An instance of this class.
   */
  public function removeMappings();

}
