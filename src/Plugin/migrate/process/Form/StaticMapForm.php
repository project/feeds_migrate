<?php

namespace Drupal\feeds_migrate\Plugin\migrate\process\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds_migrate\Plugin\MigrateFormPluginBase;

/**
 * Form for configuring the process plugin "static_map".
 *
 * @MigrateForm(
 *   id = "static_map_form",
 *   title = @Translation("Static map"),
 *   form_type = "configuration",
 *   parent_id = "static_map",
 *   parent_type = "process",
 *   category = @Translation("Text")
 * )
 */
class StaticMapForm extends MigrateFormPluginBase {

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\migrate\Plugin\migrate\process\StaticMap
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'map' => [],
      'bypass' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $map = $this->mapValuesString($this->configuration['map'], ' => ');
    $lines = explode("\n", $map);

    $list = [
      '#theme' => 'item_list',
      '#items' => $lines,
    ];

    $summary[] = $this->t('Static map: %value', [
      '%value' => \Drupal::service('renderer')->render($list),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['map'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mapping'),
      '#description' => $this->t('Enter one mapping per line, in the format from|to.'),
      '#default_value' => $this->mapValuesString($this->configuration['map']),
      '#required' => TRUE,
    ];

    $form['set_default_value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Set a default value'),
      '#default_value' => array_key_exists('default_value', $this->configuration),
    ];
    $form['default_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default value'),
      '#description' => $this->t('The fixed default value to apply.'),
      '#default_value' => $this->configuration['default_value'] ?? NULL,
    ];

    $form['bypass'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Bypass value'),
      '#description' => $this->t('When checked, if the source has a value not specified above, it will be used as is.'),
      '#default_value' => $this->configuration['bypass'],
    ];

    $form['#after_build'] = [[__CLASS__, 'afterBuild']];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Bypass and default_value may not be set both.
    if ($form_state->getValue('set_default_value') && $form_state->getValue('bypass')) {
      $form_state->setError($form['bypass'], $this->t('Setting both @default_value and @bypass is invalid.', [
        '@default_value' => $this->t('Default value'),
        '@bypass' => $this->t('Bypass value'),
      ]));
    }
  }

  /**
   * After build callback for the configuration form.
   *
   * @param array $form
   *   The built form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The complete state of the form.
   */
  public static function afterBuild(array $form, FormStateInterface $form_state) {
    // Display 'default_value' field only if 'set_default_value' is checked.
    $form['default_value']['#states'] = [
      'visible' => [
        ':input[name="' . $form['set_default_value']['#name'] . '"]' => ['checked' => TRUE],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configuration['map'] = static::extractMapValues($values['map']);
    // Only set a value for 'default_value' if 'set_default_value' is checked.
    // The static_map plugin explicitly checks if a configuration key exist for
    // 'default_value', so therefore we should not set it always.
    if ($values['set_default_value']) {
      $this->configuration['default_value'] = $values['default_value'];
    }
    else {
      unset($this->configuration['default_value']);
    }

    // Only set a value for 'bypass' if it is checked.
    if ($values['bypass']) {
      $this->configuration['bypass'] = $values['bypass'];
    }
    else {
      unset($this->configuration['bypass']);
    }
  }

  /**
   * Generates a string representation of an array of 'map'.
   *
   * This string format is suitable for edition in a textarea.
   *
   * @param array $values
   *   An array of values, where array keys are values and array values are
   *   labels.
   * @param string $separator
   *   (optional) The character to use for concatenating keys and values.
   *   Defaults to a pipe character.
   *
   * @return string
   *   The string representation of the $values array:
   *    - Values are separated by a carriage return.
   *    - Each value is in the format "value|label" or "value".
   */
  protected function mapValuesString(array $values, string $separator = '|') {
    $lines = [];
    foreach ($values as $key => $value) {
      $lines[] = "$key$separator$value";
    }
    return implode("\n", $lines);
  }

  /**
   * Extracts the mapped values array.
   *
   * @param string $string
   *   The raw string to extract values from.
   *
   * @return array|null
   *   The array of extracted key/value pairs, or NULL if the string is invalid.
   *
   * @see \Drupal\options\Plugin\Field\FieldType\ListItemBase::allowedValuesString()
   */
  protected static function extractMapValues($string) {
    $values = [];

    $list = explode("\n", $string);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');

    foreach ($list as $position => $text) {
      // Check for an explicit key.
      $matches = [];
      if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
        // Trim key and value to avoid unwanted spaces issues.
        $key = trim($matches[1]);
        $value = trim($matches[2]);
      }
      else {
        return;
      }

      $values[$key] = $value;
    }

    return $values;
  }

}
