<?php

namespace Drupal\feeds_migrate\Plugin\feeds_migrate\mapping_field;

use Drupal\feeds_migrate\MappingFieldFormBase;

/**
 * Class Default Mapping Field Form.
 *
 * @MappingFieldForm(
 *   id = "default",
 *   title = @Translation("Default Field Mapping"),
 *   fields = {}
 * )
 */
class DefaultFieldForm extends MappingFieldFormBase {

}
