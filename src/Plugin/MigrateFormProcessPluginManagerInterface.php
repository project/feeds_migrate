<?php

namespace Drupal\feeds_migrate\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\feeds_migrate\Entity\MigrationInterface;

/**
 * Interface for managing form plugins for process plugins.
 *
 * @package Drupal\feeds_migrate
 */
interface MigrateFormProcessPluginManagerInterface extends PluginManagerInterface {

  /**
   * Returns a list of available process plugins with a configuration form.
   *
   * @return array
   *   List of process plugins, keyed by plugin id.
   */
  public function getProcessPlugins();

  /**
   * Loads the process plugin.
   *
   * @param string $plugin_id
   *   The id of the process plugin.
   * @param \Drupal\feeds_migrate\Entity\MigrationInterface $migration
   *   The migration to load a form plugin for.
   * @param array $configuration
   *   The configuration for the process plugin.
   *
   * @return \Drupal\feeds_migrate\Plugin\MigrateFormPluginInterface
   *   The form process plugin instance.
   *
   * @throws \Drupal\feeds_migrate\Exception\MigrateFormPluginNotFoundException
   *   In case no form exists for the specified process plugin ID.
   */
  public function loadMigrateFormPlugin($plugin_id, MigrationInterface $migration, array $configuration = []);

}
