<?php

namespace Drupal\feeds_migrate\Plugin;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerNotInitializedException;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Error;
use Drupal\feeds_migrate\Entity\MigrationInterface;
use Drupal\feeds_migrate\Exception\MigrateFormPluginNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * Plugin manager for MigrateForm plugins for process plugins.
 *
 * @package Drupal\feeds_migrate
 */
class MigrateFormProcessPluginManager extends MigrateFormPluginManager implements MigrateFormProcessPluginManagerInterface {

  use StringTranslationTrait;

  /**
   * The Migrate process plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $processPluginManager;

  /**
   * The form factory.
   *
   * @var \Drupal\feeds_migrate\Plugin\MigrateFormPluginFactory
   */
  protected $formFactory;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new MigrateFormProcessPluginManager object.
   *
   * @param string $type
   *   The plugin type, for example data_parser, data_fetcher, destination...
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $process_plugin_manager
   *   The plugin manager for migrate process plugins.
   * @param \Drupal\feeds_migrate\Plugin\MigrateFormPluginFactory $form_factory
   *   The factory for feeds migrate form plugins.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct($type, \Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, PluginManagerInterface $process_plugin_manager, MigrateFormPluginFactory $form_factory, LoggerInterface $logger) {
    parent::__construct($type, $namespaces, $cache_backend, $module_handler);
    $this->processPluginManager = $process_plugin_manager;
    $this->formFactory = $form_factory;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessPlugins() {
    $plugins = [];
    foreach ($this->processPluginManager->getDefinitions() as $id => $definition) {
      if (!isset($definition['feeds_migrate']['form']['configuration'])) {
        // Only include process plugins which have a configuration form.
        continue;
      }

      try {
        $form_definition = $this->getDefinition($definition['feeds_migrate']['form']['configuration']);
      }
      catch (PluginNotFoundException $e) {
        // Log an exception if the form plugin cannot be found, but don't render
        // the process plugin management unusable.
        $this->logException($e);
      }

      // Get category.
      if (!empty($definition['category'])) {
        $category = $definition['category'];
      }
      if (empty($definition['category'])) {
        // Try to get category from form plugin.
        $category = $form_definition['category'] ?? $this->t('Other');
      }
      $category = (string) $category;

      // Get label.
      if (!empty($definition['label'])) {
        $label = $definition['label'];
      }
      if (empty($definition['label'])) {
        // Try to get label from form plugin.
        $label = $form_definition['title'] ?? $id;
      }

      $plugins[$category][$id] = $label;
    }

    // Don't display plugins in categories if there's only one.
    if (count($plugins) === 1) {
      $plugins = reset($plugins);
    }
    else {
      // Sort categories.
      ksort($plugins);
    }

    return $plugins;
  }

  /**
   * Logs the provided exception.
   *
   * @param \Exception $exception
   *   The exception to log.
   */
  protected function logException(\Exception $exception) {
    try {
      // Available since Drupal 10.1.0.
      if (method_exists(Error::class, 'logException')) {
        Error::logException($this->logger, $exception);
      }
      // @todo Remove when Drupal 10 is no longer supported.
      elseif (function_exists('watchdog_exception')) {
        watchdog_exception('feeds_migrate', $exception);
      }
      else {
        // Just only log the message of the exception.
        $this->logger->error($exception->getMessage());
      }
    }
    catch (ContainerNotInitializedException $e) {
      // Just only log the message of the exception.
      $this->logger->error($exception->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadMigrateFormPlugin($plugin_id, MigrationInterface $migration, array $configuration = []) {
    /** @var \Drupal\migrate\Plugin\MigrateProcessInterface $plugin */
    $plugin = $this->processPluginManager->createInstance($plugin_id, $configuration);

    // Mapping only happens during configuration.
    $operation = MigrateFormPluginInterface::FORM_TYPE_CONFIGURATION;
    if (!$this->formFactory->hasForm($plugin, $operation)) {
      throw new MigrateFormPluginNotFoundException();
    }

    return $this->formFactory->createInstance($plugin, $operation, $migration, $configuration);
  }

}
