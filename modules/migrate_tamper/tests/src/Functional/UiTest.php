<?php

namespace Drupal\Tests\migrate_tamper\Functional;

use Drupal\feeds_migrate\Entity\Migration;
use Drupal\Tests\feeds_migrate\Functional\FeedsMigrateBrowserTestBase;

/**
 * Tests configuring and display tampers in the UI.
 *
 * @group migrate_tamper
 */
class UiTest extends FeedsMigrateBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'node',
    'user',
    'migrate',
    'migrate_plus',
    'migrate_tools',
    'feeds_migrate',
    'feeds_migrate_ui',
    'feeds_migrate_test',
    'migrate_tamper',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a migration entity.
    $migration = Migration::create([
      'id' => 'example_migration',
      'label' => 'Example Migration',
      'migration_group' => 'default',
      'source' => [
        'plugin' => 'url',
        'data_fetcher_plugin' => 'http',
        'data_parser_plugin' => 'simple_xml',
      ],
      'destination' => [
        'plugin' => 'entity:node',
        'default_bundle' => 'article',
      ],
      'process' => [],
      'migration_tags' => [],
      'migration_dependencies' => [],
    ]);
    $migration->save();
  }

  /**
   * Tests displaying summary on mapping page.
   */
  public function testSummary() {
    // Programmatically add mapping to title, with tamper plugins applied.
    $migration = Migration::load('example_migration');
    $migration->set('process', [
      'title' => [
        [
          'plugin' => 'tamper:required',
          'invert' => FALSE,
        ],
        [
          'plugin' => 'tamper:trim',
          'character' => '',
          'side' => 'ltrim',
        ],
      ],
    ]);
    $migration->save();
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping');

    // Assert that the two Tamper plugins are displayed.
    $this->assertSession()->pageTextContains('Required');
    $this->assertSession()->pageTextContains('Characters to trim');
  }

}
