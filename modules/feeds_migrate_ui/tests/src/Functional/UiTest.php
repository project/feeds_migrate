<?php

namespace Drupal\Tests\feeds_migrate_ui\Functional;

/**
 * Tests if pages can be viewed and if expected links are displayed.
 *
 * @group feeds_migrate_ui
 */
class UiTest extends FeedsMigrateUiBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'file',
    'node',
    'user',
    'migrate',
    'migrate_plus',
    'migrate_tools',
    'feeds_migrate',
    'feeds_migrate_test',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Place blocks for navigation tests.
    $this->drupalPlaceBlock('page_title_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('system_menu_block:admin', [
      'expand_all_items' => TRUE,
    ]);
  }

  /**
   * Creates a user with admin privileges.
   */
  protected function createAdminUser() {
    return $this->drupalCreateUser([
      'administer feeds migrate importers',
      'administer migrations',
      'edit any article content',
      'delete any article content',
      'access administration pages',
      'link to any page',
    ]);
  }

  /**
   * Tests displaying a feeds migration overview page.
   */
  public function testFeedsMigrationOverviewPage() {
    $this->drupalGet('/admin/structure/migrate/manage/feeds_migration/migrations');

    // Test that a link for adding a migration exists.
    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    $session->linkExists('Add migration');
    $session->linkByHrefExists('/admin/structure/migrate/sources/add');
    $session->pageTextContains('There are no migration entities yet.');
  }

}
