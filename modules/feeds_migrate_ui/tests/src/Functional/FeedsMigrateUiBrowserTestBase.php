<?php

namespace Drupal\Tests\feeds_migrate_ui\Functional;

use Drupal\Tests\feeds_migrate\Functional\FeedsMigrateBrowserTestBase;

/**
 * Base class for feeds migrate UI functional tests.
 */
abstract class FeedsMigrateUiBrowserTestBase extends FeedsMigrateBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'node',
    'user',
    'migrate',
    'migrate_plus',
    'migrate_tools',
    'feeds_migrate',
    'feeds_migrate_ui',
    'feeds_migrate_test',
    'system',
  ];

}
