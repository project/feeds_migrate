<?php

namespace Drupal\Tests\feeds_migrate_ui\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests module installation.
 *
 * @group feeds_migrate_ui
 */
class InstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [];

  /**
   * Module handler, used to check which modules are installed.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  public $moduleHandler;

  /**
   * Module installer.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  public $moduleInstaller;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->moduleHandler = $this->container->get('module_handler');
    $this->moduleInstaller = $this->container->get('module_installer');
  }

  /**
   * Tests installing base module.
   */
  public function testInstallation() {
    $this->assertFalse($this->moduleHandler->moduleExists('feeds_migrate_ui'));
    $this->assertTrue($this->moduleInstaller->install(['feeds_migrate_ui']));

    unset($this->moduleHandler);
    $this->rebuildContainer();
    $this->moduleHandler = $this->container->get('module_handler');

    $this->assertTrue($this->moduleHandler->moduleExists('feeds_migrate_ui'));

    // And login as a user who may create importers.
    $account = $this->drupalCreateUser([
      'administer feeds migrate importers',
    ]);
    $this->drupalLogin($account);
  }

}
