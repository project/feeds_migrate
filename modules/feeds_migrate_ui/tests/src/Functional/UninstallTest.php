<?php

namespace Drupal\Tests\feeds_migrate_ui\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests module uninstallation.
 *
 * @group feeds_migrate_ui
 */
class UninstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['feeds_migrate_ui'];

  /**
   * Tests module uninstallation.
   */
  public function testUninstall() {
    // Confirm that feeds_migrate_ui has been installed.
    $module_handler = $this->container->get('module_handler');
    $this->assertTrue($module_handler->moduleExists('feeds_migrate_ui'));

    // Uninstall feeds_migrate_ui.
    $this->container->get('module_installer')->uninstall(['feeds_migrate_ui']);
    $this->assertFalse($module_handler->moduleExists('feeds_migrate_ui'));
  }

}
