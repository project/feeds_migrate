<?php

namespace Drupal\Tests\feeds_migrate_ui\Functional\Form;

use Drupal\feeds_migrate\Entity\Migration;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\feeds_migrate_ui\Functional\FeedsMigrateUiBrowserTestBase;

/**
 * Tests adding and editing mappings using the UI.
 *
 * @group feeds_migrate_ui
 */
class MigrationMappingFormTest extends FeedsMigrateUiBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'feeds_migrate',
    'feeds_migrate_test',
    'feeds_migrate_ui',
    'file',
    'node',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a migration entity.
    $migration = Migration::create([
      'id' => 'example_migration',
      'label' => 'Example Migration',
      'migration_group' => 'default',
      'source' => [
        'plugin' => 'url',
        'data_fetcher_plugin' => 'http',
        'data_parser_plugin' => 'simple_xml',
      ],
      'destination' => [
        'plugin' => 'entity:node',
        'default_bundle' => 'article',
      ],
      'process' => [],
      'migration_tags' => [],
      'migration_dependencies' => [],
    ]);
    $migration->save();
  }

  /**
   * Tests adding mapping without providing a source value.
   */
  public function testAddMappingWithoutSource() {
    // First, select a destination field.
    $edit = [
      'destination_field' => 'title',
    ];
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/add');
    $this->submitForm($edit, 'Save');

    // And then just save without filling in the source.
    $this->submitForm([], 'Save');

    $this->assertSession()->pageTextNotContains('Migration mapping for field Title has been updated.');
    $this->assertSession()->pageTextContains('Please enter a source or configure a process plugin.');
  }

  /**
   * Tests adding a new migration mapping with a process plugin.
   */
  public function testAddMigrationMappingWithProcessPlugin() {
    // First, select a destination field.
    $edit = [
      'destination_field' => 'title',
    ];
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/add');
    $this->submitForm($edit, 'Save');

    // Set a source and add a process plugin for it.
    $edit = [
      'mapping[title][properties][value][source]' => 'source_a',
      'mapping[title][properties][value][process][add][plugin]' => 'default_value',
    ];
    $this->submitForm($edit, 'Add');

    // Set a default value and save.
    $edit = [
      'mapping[title][properties][value][process][plugins][0][configuration][default_value]' => 'Foo',
    ];
    $this->submitForm($edit, 'Save');

    // Check if migration is saved with the expected values.
    $migration = Migration::load('example_migration');
    $expected = [
      'plugin' => 'default_value',
      'default_value' => 'Foo',
      'strict' => 0,
      'source' => 'source_a',
    ];
    $this->assertEquals($expected, $migration->get('process')['title']);
  }

  /**
   * Tests adding a process plugin for a field with more than one property.
   */
  public function testAddMigrationMappingWithProcessPluginForBodyField() {
    // Add body field.
    node_add_body_field(NodeType::load('article'));

    // First, select a destination field.
    $edit = [
      'destination_field' => 'body',
    ];
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/add');
    $this->submitForm($edit, 'Save');

    // Set a source and add a process plugin for the 'value' property.
    $edit = [
      'mapping[body][properties][value][source]' => 'source_a',
      'mapping[body][properties][value][process][add][plugin]' => 'default_value',
    ];
    $this->submitForm($edit, 'edit-mapping-body-properties-value-process-add-button');

    // Also add a process plugin for the 'summary' property.
    $edit = [
      'mapping[body][properties][summary][source]' => 'source_b',
      'mapping[body][properties][summary][process][add][plugin]' => 'default_value',
    ];
    $this->submitForm($edit, 'edit-mapping-body-properties-summary-process-add-button');

    // Configure process plugins for both properties and save.
    $edit = [
      'mapping[body][properties][value][process][plugins][0][configuration][default_value]' => 'Foo',
      'mapping[body][properties][summary][process][plugins][0][configuration][default_value]' => 'Bar',
    ];
    $this->submitForm($edit, 'Save');

    // Check if migration is saved with the expected values.
    $migration = Migration::load('example_migration');
    $expected = [
      'body/value' => [
        'plugin' => 'default_value',
        'default_value' => 'Foo',
        'strict' => 0,
        'source' => 'source_a',
      ],
      'body/summary' => [
        'plugin' => 'default_value',
        'default_value' => 'Bar',
        'strict' => 0,
        'source' => 'source_b',
      ],
    ];
    $this->assertEquals($expected, $migration->get('process'));
  }

  /**
   * Tests removing a configured process plugin.
   */
  public function testRemoveProcessPlugin() {
    // Programmatically add mapping to title, with a process plugin applied.
    $migration = Migration::load('example_migration');
    $migration->set('process', [
      'title' => [
        [
          'plugin' => 'default_value',
          'default_value' => 'Foo',
          'source' => 'source_a',
        ],
      ],
    ]);
    $migration->save();

    // Now try to remove the process plugin.
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/title/edit');
    $this->submitForm([], 'feeds-migration-mapping-,value,0');
    $this->submitForm([], 'Save');

    // Assert that the process plugin no longer exists on the mapping.
    $migration = $this->reloadEntity($migration);
    $expected = [
      'title' => 'source_a',
    ];
    $this->assertEquals($expected, $migration->get('process'));
  }

  /**
   * Tests removing multiple process plugins.
   */
  public function testRemoveMultipleProcessPlugins() {
    // Programmatically add mapping to title, with a process plugin applied.
    $migration = Migration::load('example_migration');
    $migration->set('process', [
      'title' => [
        [
          'plugin' => 'default_value',
          'default_value' => 'Foo',
          'source' => 'source_a',
        ],
        [
          'plugin' => 'default_value',
          'default_value' => 'Bar',
        ],
      ],
    ]);
    $migration->save();

    // Now try to remove the process plugin.
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/title/edit');
    $this->submitForm([], 'feeds-migration-mapping-,value,0');
    $this->submitForm([], 'feeds-migration-mapping-,value,0');
    $this->submitForm([], 'Save');

    // Assert that the process plugin no longer exists on the mapping.
    $migration = $this->reloadEntity($migration);
    $expected = [
      'title' => 'source_a',
    ];
    $this->assertEquals($expected, $migration->get('process'));
  }

  /**
   * Tests removing a process plugin for a field with more than one property.
   */
  public function testRemoveProcessPluginOnBodyField() {
    // Add body field.
    node_add_body_field(NodeType::load('article'));

    // Programmatically add mapping to body, with a process plugin applied.
    $migration = Migration::load('example_migration');
    $migration->set('process', [
      'body/value' => [
        'plugin' => 'default_value',
        'default_value' => 'Foo',
        'source' => 'body',
      ],
      'body/summary' => [
        'plugin' => 'default_value',
        'default_value' => 'Bar',
        'source' => 'summary',
      ],
    ]);
    $migration->save();

    // Now remove a process plugin for only one of the properties.
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/body/edit');
    $this->submitForm([], 'feeds-migration-mapping-,value,0');
    $this->submitForm([], 'Save');

    // Assert that the process plugin no longer exists on the mapping.
    $migration = $this->reloadEntity($migration);
    $expected = [
      'body/value' => 'body',
      'body/summary' => [
        'plugin' => 'default_value',
        'default_value' => 'Bar',
        'strict' => 0,
        'source' => 'summary',
      ],
    ];
    $this->assertEquals($expected, $migration->get('process'));
  }

  /**
   * Try to remove more than one process plugin for body field.
   */
  public function testRemoveMultipleProcessPluginsOnBodyField() {
    // Add body field.
    node_add_body_field(NodeType::load('article'));

    // Programmatically add mapping to body, with a process plugin applied.
    $migration = Migration::load('example_migration');
    $migration->set('process', [
      'body/value' => [
        'plugin' => 'default_value',
        'default_value' => 'Foo',
        'source' => 'body',
      ],
      'body/summary' => [
        'plugin' => 'default_value',
        'default_value' => 'Bar',
        'source' => 'summary',
      ],
    ]);
    $migration->save();

    // Remove process plugins for all properties.
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/body/edit');
    $this->submitForm([], 'feeds-migration-mapping-,value,0');
    $this->submitForm([], 'feeds-migration-mapping-,summary,0');
    $this->submitForm([], 'Save');

    // Assert that the process plugin no longer exists on the mapping.
    $migration = $this->reloadEntity($migration);
    $expected = [
      'body/value' => 'body',
      'body/summary' => 'summary',
    ];
    $this->assertEquals($expected, $migration->get('process'));
  }

  /**
   * Tests removing mapping for a field with more than one property.
   */
  public function testRemoveOnePropertyMapping() {
    // Add body field.
    node_add_body_field(NodeType::load('article'));

    // Programmatically add mapping for the body field.
    $migration = Migration::load('example_migration');
    $migration->set('process', [
      'body/value' => 'description',
      'body/summary' => 'excerpt',
    ]);
    $migration->save();

    // Assert that mappings are displayed with sources for both properties.
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping');
    $this->assertSession()->pageTextContains('description');
    $this->assertSession()->pageTextContains('excerpt');

    // Removing mapping for the "summary" property.
    $edit = [
      'mapping[body][properties][summary][source]' => '',
    ];
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/body/edit');
    $this->submitForm($edit, 'Save');

    // Check if migration is saved with the expected values.
    $migration = $this->reloadEntity($migration);
    $expected = [
      'body' => 'description',
    ];
    $this->assertEquals($expected, $migration->get('process'));
  }

  /**
   * Tests adding and removing mapping for a custom destination field.
   */
  public function testAddAndRemoveCustomTarget() {
    // First, select to enter a custom destination.
    $edit = [
      'destination_field' => '_custom',
      'destination_key' => 'foo',
    ];
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/add');
    $this->submitForm($edit, 'Save');

    // And set a source.
    $edit = [
      'mapping[foo][source]' => 'qux',
    ];
    $this->submitForm($edit, 'Save');

    // Check if migration is saved with the expected values.
    $migration = Migration::load('example_migration');
    $this->assertEquals('qux', $migration->get('process')['foo']);

    // Assert that the mapping is displayed with source.
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping');
    $this->assertSession()->pageTextContains('foo');
    $this->assertSession()->pageTextContains('qux');
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/foo/delete');

    // Now remove the mapper.
    $this->submitForm([], 'Delete');

    // And assert that the mapper has been removed.
    $migration = $this->reloadEntity($migration);
    $this->assertEquals([], $migration->get('process'));
  }

  /**
   * Tests displaying summary on mapping page.
   */
  public function testSummary() {
    // Programmatically add mapping to title, with three process plugins
    // applied from which one does not exist.
    $migration = Migration::load('example_migration');
    $migration->set('process', [
      'title' => [
        [
          'plugin' => 'feeds_migrate_test_foo',
          'value' => 'lorem',
        ],
        [
          'plugin' => 'feeds_migrate_test_foo',
          'value' => 'ipsum',
        ],
        [
          'plugin' => 'non_existing',
        ],
      ],
    ]);
    $migration->save();
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping');

    // Assert.
    $this->assertSession()->pageTextContains('Foo: lorem');
    $this->assertSession()->pageTextContains('Foo: ipsum');
    $this->assertSession()->pageTextContains('non_existing');
  }

}
