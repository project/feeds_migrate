<?php

namespace Drupal\Tests\feeds_migrate_ui\FunctionalJavascript;

use Drupal\Tests\feeds_migrate\FunctionalJavascript\FeedsMigrateJavascriptTestBase;

/**
 * Base class for Feeds Migrate UI javascript tests.
 */
abstract class FeedsMigrateUiJavascriptTestBase extends FeedsMigrateJavascriptTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'feeds_migrate',
    'feeds_migrate_ui',
    'file',
    'node',
    'user',
  ];

}
