<?php

namespace Drupal\feeds_migrate_test\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Dummy plugin used in Feeds Migrate tests.
 *
 * @MigrateProcessPlugin(
 *   id = "feeds_migrate_test_foo"
 * )
 */
class Foo extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return $value;
  }

}
