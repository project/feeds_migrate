<?php

namespace Drupal\feeds_migrate_test\Plugin\migrate\process\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds_migrate\Plugin\MigrateFormPluginBase;

/**
 * Form for configuring the process plugin "feeds_migrate_test_foo".
 *
 * @MigrateForm(
 *   id = "feeds_migrate_test_foo_form",
 *   title = @Translation("Foo"),
 *   form_type = "configuration",
 *   parent_id = "feeds_migrate_test_foo",
 *   parent_type = "process",
 *   category = @Translation("Text")
 * )
 */
class FooForm extends MigrateFormPluginBase {

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\feeds_migrate_test\Plugin\migrate\process\Foo
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'value' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary[] = $this->t('Foo: %value', [
      '%value' => $this->configuration['value'],
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#default_value' => $this->mapValuesString($this->configuration['value']),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configuration['value'] = $values['value'];
  }

}
