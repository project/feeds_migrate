<?php

namespace Drupal\Tests\feeds_migrate\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\feeds_migrate\Entity\MigrationInterface;
use Drupal\feeds_migrate\Exception\MigrateFormPluginNotFoundException;
use Drupal\feeds_migrate\MappingFieldFormBase;
use Drupal\feeds_migrate\Plugin\MigrateFormPluginInterface;
use Drupal\feeds_migrate\Plugin\MigrateFormProcessPluginManagerInterface;
use Prophecy\Argument;

/**
 * @coversDefaultClass \Drupal\feeds_migrate\MappingFieldFormBase
 * @group feeds_migrate
 */
class MappingFieldFormBaseTest extends FeedsMigrateUnitTestBase {

  use ProphecyTrait;
  /**
   * The migration.
   *
   * @var \Drupal\feeds_migrate\Entity\MigrationInterface
   */
  protected $migration;

  /**
   * Field Type Manager Service.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * The Migrate process plugin form manager.
   *
   * @var \Drupal\feeds_migrate\Plugin\MigrateFormProcessPluginManagerInterface
   */
  protected $processPluginFormManager;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->migration = $this->prophesize(MigrationInterface::class);
    $field_definition = $this->getMockedFieldDefinition('title', ['value']);
    $field_definition->getType()->willReturn('string');
    $this->migration->getDestinationField(Argument::type('string'))
      ->will(function ($args) use ($field_definition) {
        switch ($args[0]) {
          case 'title':
            return $field_definition->reveal();
        }
      });

    $this->fieldTypeManager = $this->prophesize(FieldTypePluginManagerInterface::class);
    $field_item = $this->prophesize(FieldItemInterface::class);
    $field_item->getProperties()
      ->willReturn(['value' => $this->createMock(TypedDataInterface::class)]);
    $this->fieldTypeManager->createInstance('string', Argument::type('array'))
      ->willReturn($field_item->reveal());

    $this->processPluginFormManager = $this->prophesize(MigrateFormProcessPluginManagerInterface::class);
  }

  /**
   * Creates a mocked MappingFieldFormBase instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return \Drupal\Tests\feeds_migrate\Unit\MappingFieldFormMock
   *   A mocked MappingFieldFormBase instance.
   */
  protected function createMappingFieldFormBase(array $configuration = [], $plugin_id = 'default', $plugin_definition = []) {
    $plugin = new MappingFieldFormMock(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $this->migration->reveal(),
      $this->fieldTypeManager->reveal(),
      $this->processPluginFormManager->reveal()
    );
    $plugin->setStringTranslation($this->getStringTranslationStub());
    $plugin->setMessenger($this->createMock(MessengerInterface::class));
    return $plugin;
  }

  /**
   * @covers ::defaultConfiguration
   *
   * @param array $expected_default_configuration
   *   The expected default configuration.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   *
   * @dataProvider providerConfiguration
   */
  public function testDefaultConfiguration(array $expected_default_configuration, array $configuration) {
    $plugin = $this->createMappingFieldFormBase($configuration);
    $this->assertEquals($expected_default_configuration, $plugin->defaultConfiguration());
  }

  /**
   * Data provider for ::testDefaultConfiguration().
   */
  public function providerConfiguration() {
    return [
      'empty config' => [
        'expected' => [],
        'config' => [],
      ],
      'with non-field' => [
        'expected' => [
          'destination_key' => 'foo',
          'mapping' => [
            'value' => [
              'destination' => [
                'key' => 'foo',
              ],
              'source' => '',
              'unique' => FALSE,
              'process' => [],
            ],
          ],
        ],
        'config' => [
          'destination_key' => 'foo',
        ],
      ],
      'with field' => [
        'expected' => [
          'destination_key' => 'title',
          'mapping' => [
            'value' => [
              'destination' => [
                'key' => 'title',
                'property' => 'value',
              ],
              'source' => '',
              'unique' => FALSE,
              'process' => [],
            ],
          ],
        ],
        'config' => [
          'destination_key' => 'title',
        ],
      ],
    ];
  }

  /**
   * Tests the configuration form.
   *
   * @param array $expected_config
   *   The expected configuration after submitting the form.
   * @param array $form_properties_values
   *   The submitted values for 'properties'.
   * @param array $form_state_init_values
   *   The form state values passed to buildConfigurationForm().
   *
   * @covers ::buildConfigurationForm
   * @covers ::buildProcessPluginsConfigurationForm
   * @covers ::buildProcessRow
   * @covers ::validateConfigurationForm
   * @covers ::submitConfigurationForm
   *
   * @dataProvider providerConfigFormValues
   */
  public function testConfigurationForm(array $expected_config, array $form_properties_values, array $form_state_init_values = []) {
    $form_plugin = $this->prophesize(MigrateFormPluginInterface::class);
    $form_plugin->buildConfigurationForm(Argument::type('array'), Argument::type(FormStateInterface::class))
      ->willReturn([]);
    $form_plugin->validateConfigurationForm(Argument::type('array'), Argument::type(FormStateInterface::class))
      ->willReturn(NULL);
    $form_plugin->submitConfigurationForm(Argument::type('array'), Argument::type(FormStateInterface::class))
      ->willReturn(NULL);
    $form_plugin->getPluginId()
      ->willReturn('default_value_form');
    $form_plugin->getConfiguration()
      ->willReturn([
        'plugin' => 'default_value',
      ]);
    $form_plugin->getPluginDefinition()
      ->willReturn([
        'id' => 'default_value_form',
        'title' => 'Default Value',
        'form_type' => 'configuration',
        'parent_id' => 'default_value',
        'parent_type' => 'process',
      ]);

    $this->processPluginFormManager->loadMigrateFormPlugin('default_value', $this->migration->reveal(), Argument::type('array'))
      ->willReturn($form_plugin->reveal());

    $this->processPluginFormManager->getProcessPlugins()->willReturn([
      'default_value' => 'default_value',
      'tamper:explode' => 'tamper:explode',
    ]);

    $plugin = $this->createMappingFieldFormBase([
      'destination_key' => 'title',
    ]);
    $form_state = new FormState();

    // The setValues() method on FormState seems to have no effect,
    // so that's why we set the property directly.
    $this->setProtectedProperty($form_state, 'values', $form_state_init_values);

    // Build the form.
    $form = $plugin->buildConfigurationForm([], $form_state);
    $this->assertIsArray($form);
    $this->assertNotEmpty($form);

    // Set values, mark the form as submitted and rebuild form.
    $form_state->setValue('properties', $form_properties_values);
    $form_state->setSubmitted();
    $form = $plugin->buildConfigurationForm([], $form_state);

    // Validate the form.
    $plugin->validateConfigurationForm($form, $form_state);

    // Submit the form.
    $plugin->submitConfigurationForm($form, $form_state);

    // Assert the expected config was set.
    $this->assertEquals($expected_config, $plugin->getConfiguration());
  }

  /**
   * Data provider for ::testConfigurationForm().
   */
  public function providerConfigFormValues() {
    $cases['simple-form-submit'] = [
      'expected_config' => [
        'destination_key' => 'title',
        'mapping' => [
          'value' => [
            'destination' => [
              'key' => 'title',
              'property' => 'value',
            ],
            'source' => 'title',
            'unique' => TRUE,
            'process' => [],
          ],
        ],
      ],
      'form_properties_values' => [
        'value' => [
          'source' => 'title',
          'unique' => 1,
          'process' => [
            'plugins' => [],
            'add' => [
              'plugin' => '',
              'button' => 'Add',
            ],
          ],
        ],
      ],
    ];

    $cases['with-single-process-plugin'] = [
      'expected_config' => [
        'destination_key' => 'title',
        'mapping' => [
          'value' => [
            'destination' => [
              'key' => 'title',
              'property' => 'value',
            ],
            'source' => 'title',
            'unique' => 1,
            'process' => [
              0 => [
                'plugin' => 'default_value',
              ],
            ],
          ],
        ],
      ],
      'form_properties_values' => [
        'value' => [
          'source' => 'title',
          'unique' => 1,
          'process' => [
            'plugins' => [
              [
                'configuration' => [
                  'plugin' => 'default_value',
                ],
                'weight' => 1,
                'operations' => 'Remove',
              ],
            ],
            'add' => [
              'plugin' => '',
              'button' => 'Add',
            ],
          ],
        ],
      ],
      'form_state_init_values' => [
        'properties' => [
          'value' => [
            'source' => 'title',
            'unique' => 1,
            'process' => [
              'plugins' => [
                [
                  'configuration' => [
                    'plugin' => 'default_value',
                  ],
                ],
              ],
              'add' => [
                'plugin' => '',
                'button' => 'Add',
              ],
            ],
          ],
        ],
      ],
    ];

    return $cases;
  }

  /**
   * Tests the configuration form for a non-field.
   *
   * @covers ::buildConfigurationForm
   * @covers ::buildProcessPluginsConfigurationForm
   * @covers ::validateConfigurationForm
   * @covers ::submitConfigurationForm
   */
  public function testConfigurationFormForNonField() {
    $plugin = $this->createMappingFieldFormBase([
      'destination_key' => 'foo',
    ]);
    $form = $plugin->buildConfigurationForm([], new FormState());
    $this->assertIsArray($form);
    $this->assertNotEmpty($form);
  }

  /**
   * @covers ::getSummary
   */
  public function testGetSummary() {
    $form_plugin = $this->prophesize(MigrateFormPluginInterface::class);
    $form_plugin->getSummary()
      ->willReturn(['Default value: Foo'])
      ->shouldBeCalled();
    $this->processPluginFormManager->loadMigrateFormPlugin('default_value', $this->migration->reveal(), Argument::type('array'))
      ->willReturn($form_plugin->reveal())
      ->shouldBeCalled();

    $config = [
      'destination_key' => 'title',
      'mapping' => [
        'value' => [
          'destination' => [
            'key' => 'title',
            'property' => 'value',
          ],
          'source' => 'title',
          'unique' => 1,
          'process' => [
            0 => [
              'plugin' => 'default_value',
            ],
            1 => [
              'plugin' => 'default_value',
            ],
          ],
        ],
      ],
    ];

    $plugin = $this->createMappingFieldFormBase($config);

    // Calling getSummary() with multiple process plugins.
    $expected = [
      'Default value: Foo',
      'Default value: Foo',
    ];
    $this->assertEquals($expected, $plugin->getSummary('value'));
  }

  /**
   * @covers ::getSummary
   */
  public function testGetSummaryWithNonExistingProcessPlugin() {
    $config = [
      'value' => [
        'destination' => [
          'key' => 'title/value',
        ],
        'source' => 'title',
        'unique' => 1,
        'process' => [
          0 => [
            'plugin' => 'foo',
          ],
        ],
      ],
      'destination' => [
        'key' => 'title',
      ],
    ];

    $plugin = $this->createMappingFieldFormBase($config);
    $this->assertEquals([], $plugin->getSummary('value'));
  }

  /**
   * @covers ::getFieldProperties
   */
  public function testGetFieldProperties() {
    $field_definition = $this->getMockedFieldDefinition('title', ['value']);
    $field_definition->getType()
      ->willReturn('string');

    $plugin = $this->createMappingFieldFormBase();

    $expected_properties = [
      'value' => $this->createMock(TypedDataInterface::class),
    ];
    $this->assertEquals($expected_properties, $this->callProtectedMethod($plugin, 'getFieldProperties', [
      $field_definition->reveal(),
    ]));
  }

  /**
   * @covers ::getFieldProperties
   */
  public function testGetFieldPropertiesFailure() {
    $field_definition = $this->getMockedFieldDefinition('title', ['value']);
    $field_definition->getType()
      ->willReturn('foo');

    $this->fieldTypeManager = $this->prophesize(FieldTypePluginManagerInterface::class);
    $this->fieldTypeManager->createInstance('foo', Argument::type('array'))
      ->willThrow(new \Exception())
      ->shouldBeCalled();

    $plugin = $this->createMappingFieldFormBase();

    $this->assertEquals([], $this->callProtectedMethod($plugin, 'getFieldProperties', [
      $field_definition->reveal(),
    ]));
  }

  /**
   * @covers ::loadProcessPlugins
   */
  public function testLoadProcessPluginsWithoutPluginConfigured() {
    $plugin = $this->createMappingFieldFormBase([
      'destination_key' => 'title',
    ]);

    $form_state = new FormState();
    $form_state->setValue('properties', [
      'value' => [
        'source' => 'title',
        'unique' => 1,
        'process' => [
          'plugins' => [],
          'add' => [
            'plugin' => '',
            'button' => 'Add',
          ],
        ],
      ],
    ]);

    $expected = [];
    $this->assertEquals($expected, $this->callProtectedMethod($plugin, 'loadProcessPlugins', [
      $form_state,
      'value',
    ]));
  }

  /**
   * @covers ::loadProcessPlugins
   */
  public function testLoadProcessPluginsWithOneProcessPluginConfigured() {
    $plugin = $this->createMappingFieldFormBase([
      'destination_key' => 'title',
    ]);

    $form_state = new FormState();
    $form_state->setValue('properties', [
      'value' => [
        'source' => 'title',
        'unique' => 1,
        'process' => [
          'plugins' => [
            [
              'configuration' => [
                'plugin' => 'default_value',
                'default_value' => 'foo',
                'strict' => 0,
              ],
              'weight' => 1,
              'operations' => 'Remove',
            ],
          ],
          'add' => [
            'plugin' => '',
            'button' => 'Add',
          ],
        ],
      ],
    ]);
    $form_state->setSubmitted();

    $expected = [
      [
        'plugin' => 'default_value',
      ],
    ];
    $this->assertEquals($expected, $this->callProtectedMethod($plugin, 'loadProcessPlugins', [
      $form_state,
      'value',
    ]));
  }

  /**
   * @covers ::loadMigrateFormPlugin
   */
  public function testLoadMigrateFormPlugin() {
    $form_plugin = $this->createMock(MigrateFormPluginInterface::class);
    $this->processPluginFormManager->loadMigrateFormPlugin('default_value', $this->migration->reveal(), [])
      ->willReturn($form_plugin);

    $plugin = $this->createMappingFieldFormBase();
    $this->assertEquals($form_plugin, $this->callProtectedMethod($plugin, 'loadMigrateFormPlugin', ['default_value']));
  }

  /**
   * @covers ::loadMigrateFormPlugin
   */
  public function testLoadMigrateFormPluginWithPluginNotFound() {
    $this->processPluginFormManager->loadMigrateFormPlugin('default_value', $this->migration->reveal(), [])
      ->willThrow(new MigrateFormPluginNotFoundException())
      ->shouldBeCalled();

    $plugin = $this->createMappingFieldFormBase();
    $this->assertNull($this->callProtectedMethod($plugin, 'loadMigrateFormPlugin', ['default_value']));
  }

  /**
   * @covers ::loadMigrateFormPlugin
   *
   * Exceptions of type PluginException should be caught.
   */
  public function testLoadMigrateFormPluginWithPluginException() {
    $this->processPluginFormManager->loadMigrateFormPlugin('foo', $this->migration->reveal(), [])
      ->willThrow(new PluginException())
      ->shouldBeCalled();

    $plugin = $this->createMappingFieldFormBase();
    $this->assertNull($this->callProtectedMethod($plugin, 'loadMigrateFormPlugin', ['foo']));
  }

}

/**
 * Mock for abstract class MappingFieldFormBase.
 */
class MappingFieldFormMock extends MappingFieldFormBase {}
