<?php

namespace Drupal\Tests\feeds_migrate\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\feeds_migrate\Entity\MigrationInterface;
use Drupal\feeds_migrate\MappingFieldFormManager;
use Drupal\feeds_migrate\Plugin\feeds_migrate\mapping_field\DefaultFieldForm;
use Drupal\feeds_migrate\Plugin\feeds_migrate\mapping_field\TextAreaFieldForm;
use Drupal\feeds_migrate\Plugin\MigrateFormPluginFactory;
use Drupal\feeds_migrate\Plugin\MigrateFormProcessPluginManagerInterface;

/**
 * @coversDefaultClass \Drupal\feeds_migrate\MappingFieldFormManager
 * @group feeds_migrate
 */
class MappingFieldFormManagerTest extends FeedsMigrateUnitTestBase {

  use ProphecyTrait;
  /**
   * The mapping field form manager to test.
   *
   * @var \Drupal\feeds_migrate\MappingFieldFormManager
   */
  protected $manager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\Prophecy\Prophecy\ProphecyInterface
   */
  protected $moduleHandler;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface|\Prophecy\Prophecy\ProphecyInterface
   */
  protected $cacheBackend;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $namespaces = new \ArrayObject();
    $this->cacheBackend = $this->prophesize(CacheBackendInterface::class);
    $this->moduleHandler = $this->prophesize(ModuleHandlerInterface::class);

    $this->manager = $this->buildMappingFieldFormManagerMock()
      ->setMethods(['findDefinitions'])
      ->getMock();
    $this->manager->expects($this->once())
      ->method('findDefinitions')
      ->willReturn([
        'default' => [
          'id' => 'default',
          'title' => 'Default Field Mapping',
          'fields' => [],
          'class' => DefaultFieldForm::class,
          'provider' => 'feeds_migrate',
        ],
        'text_area' => [
          'id' => 'text_area',
          'title' => 'Field mapping for textareas',
          'fields' => [
            'text_long',
            'text_with_summary',
          ],
          'class' => TextAreaFieldForm::class,
          'provider' => 'feeds_migrate',
        ],
      ]);
  }

  /**
   * Builds a mock for the mapping field form manager.
   *
   * @return \PHPUnit\Framework\MockObject\MockBuilder
   *   The mock builder, building a mock for the mapping field form manager.
   */
  protected function buildMappingFieldFormManagerMock() {
    return $this->getMockBuilder(MappingFieldFormManager::class)
      ->setConstructorArgs([
        new \ArrayObject(),
        $this->cacheBackend->reveal(),
        $this->moduleHandler->reveal(),
      ]);
  }

  /**
   * @covers ::getPluginIdFromField
   *
   * @param string $expected_plugin_id
   *   The plugin ID the method is expected to return.
   * @param string $field_type
   *   The field type of the field definition that is passed.
   *
   * @dataProvider providerPluginIdFromField
   */
  public function testGetPluginIdFromField($expected_plugin_id, $field_type) {
    $field_definition = $this->prophesize(FieldDefinitionInterface::class);
    $field_definition->getType()
      ->willReturn($field_type);

    $this->assertEquals($expected_plugin_id, $this->manager->getPluginIdFromField($field_definition->reveal()));
  }

  /**
   * Data provider for ::testGetPluginIdFromField().
   */
  public function providerPluginIdFromField() {
    return [
      // Plugin ID is 'default' when there is no specific plugin for the
      // field type.
      ['default', 'custom_field_type'],
      // For some field types, there is a specific plugin.
      ['text_area', 'text_with_summary'],
    ];
  }

  /**
   * @covers ::createInstance
   */
  public function testCreateInstance() {
    // The plugin manager needs the Drupal service container to create a plugin
    // instance.
    $container = new ContainerBuilder();
    $container->set('plugin.manager.migrate.process', $this->createMock(PluginManagerInterface::class));
    $container->set('plugin.manager.field.field_type', $this->createMock(FieldTypePluginManagerInterface::class));
    $container->set('feeds_migrate.migrate_form_plugin_factory', $this->createMock(MigrateFormPluginFactory::class));
    $container->set('plugin.manager.feeds_migrate.migrate.process_form', $this->createMock(MigrateFormProcessPluginManagerInterface::class));
    \Drupal::setContainer($container);

    // Create default plugin.
    $plugin = $this->manager->createInstance('default', [], $this->createMock(MigrationInterface::class));
    $this->assertInstanceOf(DefaultFieldForm::class, $plugin);
  }

  /**
   * @covers ::createInstance
   */
  public function testCreateInstanceException() {
    // Create non-existent plugin.
    $this->expectException(PluginNotFoundException::class);
    $this->manager->createInstance('non_existent', [], $this->createMock(MigrationInterface::class));
  }

}
