<?php

namespace Drupal\Tests\feeds_migrate\Unit\Plugin;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\feeds_migrate\Entity\MigrationInterface;
use Drupal\feeds_migrate\Exception\MigrateFormPluginNotFoundException;
use Drupal\feeds_migrate\Plugin\MigrateFormPluginFactory;
use Drupal\feeds_migrate\Plugin\MigrateFormPluginInterface;
use Drupal\feeds_migrate\Plugin\MigrateFormProcessPluginManager;
use Drupal\migrate\Plugin\MigrateProcessInterface;
use Drupal\Tests\feeds_migrate\Unit\FeedsMigrateUnitTestBase;
use Psr\Log\LoggerInterface;

/**
 * @coversDefaultClass \Drupal\feeds_migrate\Plugin\MigrateFormProcessPluginManager
 * @group feeds_migrate
 */
class MigrateFormProcessPluginManagerTest extends FeedsMigrateUnitTestBase {

  use ProphecyTrait;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\Prophecy\Prophecy\ProphecyInterface
   */
  protected $moduleHandler;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface|\Prophecy\Prophecy\ProphecyInterface
   */
  protected $cacheBackend;

  /**
   * The Migrate process plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $processPluginManager;

  /**
   * The form factory.
   *
   * @var \Drupal\feeds_migrate\Plugin\MigrateFormPluginFactory
   */
  protected $formFactory;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $namespaces = new \ArrayObject();
    $this->cacheBackend = $this->prophesize(CacheBackendInterface::class);
    $this->moduleHandler = $this->prophesize(ModuleHandlerInterface::class);

    $this->processPluginManager = $this->prophesize(PluginManagerInterface::class);
    $this->processPluginManager->getDefinitions()
      ->willReturn([
        'default_value' => [
          'handle_multiples' => TRUE,
          'id' => 'default_value',
          'class' => 'Drupal\migrate\Plugin\migrate\process\DefaultValue',
          'provider' => 'migrate',
          'feeds_migrate' => [
            'form' => [
              'configuration' => 'default_value_form',
            ],
          ],
        ],
      ]);

    $this->formFactory = $this->prophesize(MigrateFormPluginFactory::class);
    $this->logger = $this->prophesize(LoggerInterface::class);
  }

  /**
   * Creates a mock for the process plugin form manager.
   *
   * @return \Drupal\feeds_migrate\MigrateFormProcessPluginManager
   *   An instance of MigrateFormProcessPluginManager.
   */
  protected function createProcessPluginFormManager() {
    $manager = new MigrateFormProcessPluginManager(
      'process',
      new \ArrayObject(),
      $this->cacheBackend->reveal(),
      $this->moduleHandler->reveal(),
      $this->processPluginManager->reveal(),
      $this->formFactory->reveal(),
      $this->logger->reveal()
    );
    $manager->setStringTranslation($this->getStringTranslationStub());
    return $manager;
  }

  /**
   * @covers ::getProcessPlugins
   */
  public function testGetProcessPlugins() {
    $manager = $this->createProcessPluginFormManager();
    $expected = [
      'default_value' => 'default_value',
    ];
    $this->assertEquals($expected, $manager->getProcessPlugins());
  }

  /**
   * @covers ::getProcessPlugins
   */
  public function testGetProcessPluginsWithMultiplePlugins() {
    $this->processPluginManager = $this->prophesize(PluginManagerInterface::class);
    $this->processPluginManager->getDefinitions()
      ->willReturn([
        'foo' => [
          'handle_multiples' => FALSE,
          'id' => 'foo',
          'class' => 'Drupal\foo\Plugin\migrate\process\Foo',
          'provider' => 'foo',
        ],
        'default_value' => [
          'handle_multiples' => TRUE,
          'id' => 'default_value',
          'class' => 'Drupal\migrate\Plugin\migrate\process\DefaultValue',
          'provider' => 'migrate',
          'feeds_migrate' => [
            'form' => [
              'configuration' => 'default_value_form',
            ],
          ],
        ],
        'tamper:explode' => [
          'handle_multiples' => TRUE,
          'id' => 'tamper',
          'deriver' => 'Drupal\migrate_tamper\Plugin\Derivative\TamperProcessPluginDeriver',
          'class' => 'Drupal\migrate_tamper\Plugin\migrate\process\Tamper',
          'provider' => 'tamper',
          'category' => 'List',
          'label' => 'Explode',
          'tamper_plugin_id' => 'explode',
          'feeds_migrate' => [
            'form' => [
              'configuration' => 'tamper_form',
            ],
          ],
        ],
        'tamper:implode' => [
          'handle_multiples' => TRUE,
          'id' => 'tamper',
          'deriver' => 'Drupal\migrate_tamper\Plugin\Derivative\TamperProcessPluginDeriver',
          'class' => 'Drupal\migrate_tamper\Plugin\migrate\process\Tamper',
          'provider' => 'tamper',
          'category' => 'List',
          'label' => 'Implode',
          'tamper_plugin_id' => 'implode',
          'feeds_migrate' => [
            'form' => [
              'configuration' => 'tamper_form',
            ],
          ],
        ],
      ]);

    $manager = $this->createProcessPluginFormManager();
    $expected = [
      'List' => [
        'tamper:explode' => 'Explode',
        'tamper:implode' => 'Implode',
      ],
      'Other' => [
        'default_value' => 'default_value',
      ],
    ];
    $this->assertEquals($expected, $manager->getProcessPlugins());
  }

  /**
   * @covers ::loadMigrateFormPlugin
   */
  public function testLoadMigrateFormPlugin() {
    $migration = $this->createMock(MigrationInterface::class);
    $form_plugin = $this->createMock(MigrateFormPluginInterface::class);

    $process_plugin = $this->createMock(MigrateProcessInterface::class);
    $this->processPluginManager->createInstance('default_value', [])
      ->willReturn($process_plugin);
    $this->formFactory->hasForm($process_plugin, MigrateFormPluginInterface::FORM_TYPE_CONFIGURATION)
      ->willReturn(TRUE);
    $this->formFactory->createInstance($process_plugin, MigrateFormPluginInterface::FORM_TYPE_CONFIGURATION, $migration, [])
      ->willReturn($form_plugin);

    $manager = $this->createProcessPluginFormManager();
    $this->assertEquals($form_plugin, $manager->loadMigrateFormPlugin('default_value', $migration));
  }

  /**
   * @covers ::loadMigrateFormPlugin
   */
  public function testLoadMigrateFormPluginWithPluginNotFound() {
    $migration = $this->createMock(MigrationInterface::class);
    $process_plugin = $this->createMock(MigrateProcessInterface::class);
    $this->processPluginManager->createInstance('default_value', [])
      ->willReturn($process_plugin);
    $this->formFactory->hasForm($process_plugin, MigrateFormPluginInterface::FORM_TYPE_CONFIGURATION)
      ->willReturn(FALSE);

    $manager = $this->createProcessPluginFormManager();
    $this->expectException(MigrateFormPluginNotFoundException::class);
    $manager->loadMigrateFormPlugin('default_value', $migration);
  }

}
