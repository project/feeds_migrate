<?php

namespace Drupal\Tests\feeds_migrate\Unit\Plugin\feeds_migrate\mapping_field;

use Drupal\feeds_migrate\Plugin\feeds_migrate\mapping_field\TextAreaFieldForm;

/**
 * @coversDefaultClass \Drupal\feeds_migrate\Plugin\feeds_migrate\mapping_field\TextAreaFieldForm
 * @group feeds_migrate
 */
class TextAreaFieldFormTest extends MappingFieldFormTestBase {

  /**
   * {@inheritdoc}
   */
  protected function getPluginDefinition() {
    return [
      'id' => 'text_area',
      'title' => 'Field mapping for textareas',
      'fields' => [
        'text_long',
        'text_with_summary',
      ],
      'class' => TextAreaFieldForm::class,
      'provider' => 'feeds_migrate',
    ];
  }

}
