<?php

namespace Drupal\Tests\feeds_migrate\Unit\Plugin\feeds_migrate\mapping_field;

use Drupal\feeds_migrate\Plugin\feeds_migrate\mapping_field\DefaultFieldForm;

/**
 * @coversDefaultClass \Drupal\feeds_migrate\Plugin\feeds_migrate\mapping_field\DefaultFieldForm
 * @group feeds_migrate
 */
class DefaultFieldFormTest extends MappingFieldFormTestBase {

  /**
   * {@inheritdoc}
   */
  protected function getPluginDefinition() {
    return [
      'id' => 'default',
      'title' => 'Default Field Mapping',
      'fields' => [],
      'class' => DefaultFieldForm::class,
      'provider' => 'feeds_migrate',
    ];
  }

}
