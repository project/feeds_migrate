<?php

namespace Drupal\Tests\feeds_migrate\Unit\Plugin\feeds_migrate\mapping_field;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\feeds_migrate\Entity\MigrationInterface;
use Drupal\feeds_migrate\Plugin\MigrateFormProcessPluginManagerInterface;
use Drupal\Tests\feeds_migrate\Unit\FeedsMigrateUnitTestBase;
use Prophecy\Argument;

/**
 * Base class for MappingFieldForm plugins.
 */
abstract class MappingFieldFormTestBase extends FeedsMigrateUnitTestBase {

  use ProphecyTrait;
  /**
   * The MappingFieldForm plugin under test.
   *
   * @var \Drupal\feeds_migrate\MappingFieldFormInterface
   */
  protected $plugin;

  /**
   * The migration.
   *
   * @var \Drupal\feeds_migrate\Entity\MigrationInterface
   */
  protected $migration;

  /**
   * Field Type Manager Service.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * The Migrate process plugin form manager.
   *
   * @var \Drupal\feeds_migrate\Plugin\MigrateFormProcessPluginManagerInterface
   */
  protected $processPluginFormManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->migration = $this->prophesize(MigrationInterface::class);
    $field_definition = $this->getMockedFieldDefinition('title', ['value']);
    $field_definition->getType()->willReturn('string');
    $field_definition->getLabel()->willReturn('foo');
    $this->migration->getDestinationField(Argument::type('string'))
      ->will(function ($args) use ($field_definition) {
        switch ($args[0]) {
          case 'title':
            return $field_definition->reveal();
        }
      });

    $this->fieldTypeManager = $this->prophesize(FieldTypePluginManagerInterface::class);
    $field_item = $this->prophesize(FieldItemInterface::class);
    $field_item->getProperties()
      ->willReturn(['value' => $this->createMock(TypedDataInterface::class)]);
    $this->fieldTypeManager->createInstance('string', Argument::type('array'))
      ->willReturn($field_item->reveal());

    $this->processPluginFormManager = $this->prophesize(MigrateFormProcessPluginManagerInterface::class);

    $this->plugin = $this->instantiatePlugin([
      'destination_key' => 'title',
    ]);
  }

  /**
   * Instantiates the plugin to test.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   *
   * @return \Drupal\feeds_migrate\MappingFieldFormInterface
   *   A MappingFieldForm plugin.
   */
  protected function instantiatePlugin(array $configuration = []) {
    $plugin_definition = $this->getPluginDefinition();
    $plugin_class = $plugin_definition['class'];

    $plugin = new $plugin_class(
      $configuration,
      $plugin_definition['id'],
      $plugin_definition,
      $this->migration->reveal(),
      $this->fieldTypeManager->reveal(),
      $this->processPluginFormManager->reveal()
    );
    $plugin->setStringTranslation($this->getStringTranslationStub());
    return $plugin;
  }

  /**
   * Returns the definition of the plugin to test.
   *
   * The definition should at least include the following properties:
   * - id
   *   The plugin's ID.
   * - class
   *   The plugin's class to instantiate.
   *
   * @return array
   *   The definition of the plugin to test.
   */
  abstract protected function getPluginDefinition();

  /**
   * @covers ::getPluginId
   */
  public function testGetPluginId() {
    $this->assertIsString($this->plugin->getPluginId());
  }

  /**
   * @covers ::getPluginDefinition
   */
  public function testGetPluginDefinition() {
    $this->assertIsArray($this->plugin->getPluginDefinition());
  }

  /**
   * @covers ::getConfiguration
   */
  public function testGetConfiguration() {
    $this->assertIsArray($this->plugin->getConfiguration());
  }

  /**
   * @covers ::setConfiguration
   * @covers ::getConfiguration
   * @covers ::defaultConfiguration
   */
  public function testSetConfiguration() {
    $this->plugin->setConfiguration([
      'mapping' => [
        'value' => [
          'destination' => [
            'foo' => 'bar',
          ],
        ],
      ],
    ]);

    $expected = [
      'destination_key' => 'title',
      'mapping' => [
        'value' => [
          'destination' => [
            'key' => 'title',
            'property' => 'value',
            'foo' => 'bar',
          ],
          'source' => '',
          'unique' => FALSE,
          'process' => [],
        ],
      ],
    ];
    $this->assertEquals($expected, $this->plugin->getConfiguration());
  }

  /**
   * @covers ::defaultConfiguration
   */
  public function testDefaultConfiguration() {
    $this->assertIsArray($this->plugin->defaultConfiguration());
  }

  /**
   * @covers ::buildConfigurationForm
   */
  public function testBuildConfigurationForm() {
    $form_state = new FormState();

    $this->assertIsArray($this->plugin->buildConfigurationForm([], $form_state));
  }

  /**
   * @covers ::getLabel
   */
  public function testGetLabel() {
    $this->assertIsString($this->plugin->getLabel());
  }

}
