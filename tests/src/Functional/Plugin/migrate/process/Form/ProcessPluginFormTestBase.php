<?php

namespace Drupal\Tests\feeds_migrate\Functional\Plugin\migrate\process\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\feeds_migrate\Entity\Migration;
use Drupal\Tests\feeds_migrate\Functional\FeedsMigrateBrowserTestBase;

/**
 * Tests configuring process plugins in the UI.
 */
abstract class ProcessPluginFormTestBase extends FeedsMigrateBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'node',
    'user',
    'migrate',
    'migrate_plus',
    'migrate_tools',
    'feeds_migrate',
    'feeds_migrate_ui',
    'feeds_migrate_test',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a migration entity.
    $migration = Migration::create([
      'id' => 'example_migration',
      'label' => 'Example Migration',
      'migration_group' => 'default',
      'source' => [
        'plugin' => 'url',
        'data_fetcher_plugin' => 'http',
        'data_parser_plugin' => 'simple_xml',
      ],
      'destination' => [
        'plugin' => 'entity:node',
        'default_bundle' => 'article',
      ],
      'process' => [],
      'migration_tags' => [],
      'migration_dependencies' => [],
    ]);
    $migration->save();
  }

  /**
   * Tests adding a process plugin of the given type.
   *
   * @param array $expected_config
   *   The expected saved configuration.
   * @param array $config
   *   The config for the process plugin.
   * @param array $errors
   *   The form validation errors displayed on the page.
   *
   * @dataProvider formDataProvider
   */
  public function testForm(array $expected_config, array $config = [], array $errors = []) {
    $expected = $expected_config + [
      'plugin' => static::$pluginId,
      'source' => 'source_a',
    ];

    // First, select a destination field.
    $edit = [
      'destination_field' => 'title',
    ];
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/add');
    $this->submitForm($edit, 'Save');

    // Set a source and add a process plugin for it.
    $edit = [
      'mapping[title][properties][value][source]' => 'source_a',
      'mapping[title][properties][value][process][add][plugin]' => static::$pluginId,
    ];
    $this->submitForm($edit, 'Add');

    // Set edit values.
    $edit = [];
    foreach ($config as $key => $value) {
      $edit["mapping[title][properties][value][process][plugins][0][configuration][$key]"] = $value;
    }
    $this->submitForm($edit, 'Save');

    // Check for texts on the page.
    $session = $this->assertSession();
    if (!empty($errors)) {
      $session->pageTextNotContains('Migration mapping for field Title has been updated.');
      foreach ($errors as $error) {
        $session->pageTextContains($error);
      }

      // Abort the test here.
      return;
    }
    else {
      $session->pageTextContains('Migration mapping for field Title has been updated.');
    }

    // Check if migration is saved with the expected values.
    $migration = Migration::load('example_migration');
    $this->assertEquals($expected, $migration->get('process')['title']);

    // Submit the form again with no values and assert that the plugin is still
    // configured the same.
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/title/edit');
    $this->submitForm([], 'Save');
    $migration = $this->reloadEntity($migration);
    $this->assertEquals($expected, $migration->get('process')['title']);

    // Try to add a second process plugin of the same type.
    $this->drupalGet('/admin/structure/migrate/sources/example_migration/mapping/title/edit');
    $edit = [
      'mapping[title][properties][value][process][add][plugin]' => static::$pluginId,
    ];
    $this->submitForm($edit, 'Add');

    // Set edit values.
    $edit = [];
    foreach ($config as $key => $value) {
      $edit["mapping[title][properties][value][process][plugins][1][configuration][$key]"] = $value;
    }
    $this->submitForm($edit, 'Save');

    // Check if the migration is saved with the expected values now.
    $migration = $this->reloadEntity($migration);
    $expected = [
      [
        'plugin' => static::$pluginId,
        'source' => 'source_a',
      ] + $expected_config,
      [
        'plugin' => static::$pluginId,
      ] + $expected_config,
    ];
    $this->assertEquals($expected, $migration->get('process')['title']);
  }

  /**
   * Data provider for ::testForm().
   */
  public function formDataProvider(): array {
    // Some plugins don't have special configuration.
    return [
      'no values' => [
        'expected' => [],
      ],
    ];
  }

  /**
   * Reloads an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to reload.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The reloaded entity.
   */
  protected function reloadEntity(EntityInterface $entity) {
    /** @var \Drupal\Core\Entity\ $storageEntityStorageInterface */
    $storage = $this->container->get('entity_type.manager')->getStorage($entity->getEntityTypeId());
    $storage->resetCache([$entity->id()]);
    return $storage->load($entity->id());
  }

}
