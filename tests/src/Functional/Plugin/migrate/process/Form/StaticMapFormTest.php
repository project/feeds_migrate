<?php

namespace Drupal\Tests\feeds_migrate\Functional\Plugin\migrate\process\Form;

/**
 * Tests the form of the static map process plugin.
 *
 * @coversDefaultClass \Drupal\feeds_migrate\Plugin\migrate\process\Form\StaticMapForm
 * @group feeds_migrate
 */
class StaticMapFormTest extends ProcessPluginFormTestBase {

  /**
   * The ID of the plugin to test.
   *
   * @var string
   */
  protected static $pluginId = 'static_map';

  /**
   * {@inheritdoc}
   */
  public function formDataProvider(): array {
    return [
      'no values' => [
        'expected' => [],
        'edit' => [],
        'errors' => [
          'Mapping field is required.',
        ],
      ],
      'mapping values' => [
        'expected' => [
          'map' => [
            'foo' => 'bar',
            'qux' => 'thud',
          ],
        ],
        'edit' => [
          'map' => "foo|bar\nqux|thud",
        ],
      ],
      'with default value' => [
        'expected' => [
          'map' => [
            'foo' => 'bar',
            'qux' => 'thud',
          ],
          'default_value' => 'Foo',
        ],
        'edit' => [
          'map' => "foo|bar\nqux|thud",
          'set_default_value' => TRUE,
          'default_value' => 'Foo',
        ],
      ],
      'with default value, but set_default_value disabled' => [
        'expected' => [
          'map' => [
            'foo' => 'bar',
            'qux' => 'thud',
          ],
        ],
        'edit' => [
          'map' => "foo|bar\nqux|thud",
          'set_default_value' => FALSE,
          'default_value' => 'Foo',
        ],
      ],
      'with bypass enabled' => [
        'expected' => [
          'map' => [
            'foo' => 'bar',
            'qux' => 'thud',
          ],
          'bypass' => TRUE,
        ],
        'edit' => [
          'map' => "foo|bar\nqux|thud",
          'bypass' => 1,
        ],
      ],
      'with both set_default_value and bypass enabled' => [
        'expected' => [],
        'edit' => [
          'map' => "foo|bar\nqux|thud",
          'set_default_value' => TRUE,
          'default_value' => 'Foo',
          'bypass' => 1,
        ],
        'errors' => [
          'Setting both Default value and Bypass value is invalid.',
        ],
      ],
    ];
  }

}
