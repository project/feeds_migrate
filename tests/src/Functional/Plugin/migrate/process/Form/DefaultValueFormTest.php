<?php

namespace Drupal\Tests\feeds_migrate\Functional\Plugin\migrate\process\Form;

/**
 * Tests the form of the default value process plugin.
 *
 * @coversDefaultClass \Drupal\feeds_migrate\Plugin\migrate\process\Form\DefaultValueForm
 * @group feeds_migrate
 */
class DefaultValueFormTest extends ProcessPluginFormTestBase {

  /**
   * The ID of the plugin to test.
   *
   * @var string
   */
  protected static $pluginId = 'default_value';

  /**
   * {@inheritdoc}
   */
  public function formDataProvider(): array {
    return [
      'no values' => [
        'expected' => [
          'default_value' => '',
          'strict' => FALSE,
        ],
      ],
      'with values' => [
        'expected' => [
          'default_value' => 'A default',
          'strict' => TRUE,
        ],
        'edit' => [
          'default_value' => 'A default',
          'strict' => 1,
        ],
      ],
    ];
  }

}
