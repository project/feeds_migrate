<?php

namespace Drupal\Tests\feeds_migrate\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests module uninstallation.
 *
 * @group feeds_migrate
 */
class UninstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['feeds_migrate'];

  /**
   * Tests module uninstallation.
   */
  public function testUninstall() {
    // Confirm that feeds_migrate has been installed.
    $module_handler = $this->container->get('module_handler');
    $this->assertTrue($module_handler->moduleExists('feeds_migrate'));

    // Uninstall feeds_migrate.
    $this->container->get('module_installer')->uninstall(['feeds_migrate']);
    $this->assertFalse($module_handler->moduleExists('feeds_migrate'));
  }

}
