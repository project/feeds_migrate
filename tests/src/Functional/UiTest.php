<?php

namespace Drupal\Tests\feeds_migrate\Functional;

/**
 * Tests if pages can be viewed and if expected links are displayed.
 *
 * @group feeds_migrate
 */
class UiTest extends FeedsMigrateBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'file',
    'node',
    'user',
    'migrate',
    'migrate_plus',
    'migrate_tools',
    'feeds_migrate',
    'feeds_migrate_test',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Place blocks for navigation tests.
    $this->drupalPlaceBlock('page_title_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('system_menu_block:admin', [
      'expand_all_items' => TRUE,
    ]);
  }

  /**
   * Creates a user with admin privileges.
   */
  protected function createAdminUser() {
    return $this->drupalCreateUser([
      'administer feeds migrate importers',
      'administer migrations',
      'edit any article content',
      'delete any article content',
      'access administration pages',
      'link to any page',
    ]);
  }

  /**
   * Tests displaying an importer overview page.
   */
  public function testImporterOverviewPage() {
    $this->drupalGet('/admin/content/feeds-migrate');

    // Test that a link to adding an importer is displayed.
    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    $session->linkExists('Add importer');
    $session->linkByHrefExists('/admin/content/feeds-migrate/importer/add');
  }

  /**
   * Tests displaying a feeds migration overview page.
   */
  public function testFeedsMigrationOverviewPage() {
    $this->drupalGet('/admin/structure/migrate');
    $this->drupalGet('/admin/structure/migrate/manage/feeds_migration/migrations');

    // Test that no link for adding a migration exists, because that is a job of
    // the feeds_migrate_ui module.
    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    $session->linkNotExists('Add migration');
    $session->linkByHrefNotExists('/admin/structure/migrate/sources/add');
    $session->pageTextContains('There are no migration entities yet.');
  }

}
